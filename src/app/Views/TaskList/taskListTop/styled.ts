import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 10px auto;
  max-width: 750px;

  & > div {
    padding: 5px 15px 15px;
  }
`;