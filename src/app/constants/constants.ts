export const SET_USER = 'SET_USER';
export const SET_STATE = 'SET_STATE';
export const SET_FLOW = 'SET_FLOW';
export const TOGGLE_DONE = 'TOGGLE_DONE';
export const ADD_TASK = 'ADD_TASK';
export const CHANGE_TASK = 'CHANGE_TASK';
export const DEL_TASK = 'DEL_TASK';
export const EDIT_TASK = 'EDIT_TASK';
export const CLEAR_TASKS = 'CLEAR_TASKS';

export const FONTS_STACK = Object.freeze({
    ROBOTO: 'Roboto, sans-serif',
    RUSSOONE: 'RussoOne'
});