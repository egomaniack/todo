export interface Task {
    id: number;
    title: string;
    body: string;
    creationTimeStamp: Date;
    done: boolean;
}